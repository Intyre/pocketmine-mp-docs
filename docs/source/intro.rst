.. _intro:

Introduction
============
.. image:: img/PocketMine.png
	:align: center
	
PocketMine-MP is a server software for Minecraft PE (Pocket Edition).
It has a :doc:`Plugin API <plugins>` that enables a :doc:`developer <developers>` to extend it and add new features, or change default ones.

Supported features
------------------
* Get all your friends in one server. Or run a public server.
* Disables flying, item hack, running & more. With an On/Off switch.
* Extend the game in the way you want, add awesome features.
* Teleport players, whitelist your server, tune the server, Remote Console.
* Load different levels at once, and teleport back and forth.
* Endless features, and we continuously implement new things.

Contact and Support
-------------------
* `@PocketMine <https://twitter.com/PocketMine>`_ on twitter
* `#pocketmine <http://webchat.freenode.net/?channels=pocketmine&uio=d4>`_ for support @ irc.freenode.net
* `#mcpedevs <http://webchat.freenode.net/?channels=mcpedevs&uio=d4>`_ for development @ irc.freenode.net
* Mailing list: `Discussion group <https://groups.google.com/forum/#!forum/pocketmine-user-discussion>`_

