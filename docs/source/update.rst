.. _update:

Updating
========

.. contents::
	:local:
	:depth: 2

Manually update
---------------

Update PHP binary
+++++++++++++++++

Download the PHP binary for your OS:

* `Windows <PHP-Windows_>`_
* `MacOS <PHP-SourceForge_>`_
* `CentOS <PHP-SourceForge_>`_
* `Linux <PHP-SourceForge_>`_ or `Jenkins <PHP-Jenkins_>`_
* `Linux ARM <PHP-SourceForge_>`_ or `Jenkins <PHP-Jenkins_>`_
* `Android <PHP-SourceForge_>`_ or `Jenkins <PHP-Jenkins_>`_
* `Raspbian <PHP-SourceForge_>`_

Update PocketMine-MP
++++++++++++++++++++

Download the latest `stable <PM-Stable_>`_ or `development <PM-Dev_>`_ release .

.. note:: Dont forget to rename the file to ``PocketMine-MP.phar``

.. _PHP-Windows: http://sourceforge.net/projects/pocketmine/files/windows/dev/
.. _PHP-SourceForge: http://sourceforge.net/projects/pocketmine/files/builds/
.. _PHP-Jenkins: http://jenkins.pocketmine.net/
.. _PM-Stable: https://github.com/PocketMine/PocketMine-MP/releases
.. _PM-Dev: http://jenkins.pocketmine.net/job/PocketMine-MP/
