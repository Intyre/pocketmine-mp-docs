.. image:: img/PocketMine.png
   :align: center

PocketMine Documentation
========================

.. toctree::
   :maxdepth: 2

   intro
   require
   setup
   config
   update
   faq
   developers
   plugins
   issues
   