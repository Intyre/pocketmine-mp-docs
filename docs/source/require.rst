.. _require:

Requirements
============
A few extra extensions are needed to run PocketMine-MP with php. Thats why it does not work on standard php binaries.
You can get the latest versions of the binaries from a few places

Custom PHP binaries
-------------------
* `Windows <PHP-Windows_>`_
* `MacOS <PHP-SourceForge_>`_
* `CentOS <PHP-SourceForge_>`_
* `Linux <PHP-SourceForge_>`_ or `Jenkins <PHP-Jenkins_>`_
* `Linux ARM <PHP-SourceForge_>`_ or `Jenkins <PHP-Jenkins_>`_
* `Android <PHP-SourceForge_>`_ or `Jenkins <PHP-Jenkins_>`_
* `Raspbian <PHP-SourceForge_>`_

Third-party Libraries/Protocols Used
------------------------------------
* `PHP Sockets <http://php.net/manual/en/book.sockets.php>`_
* `PHP SQLite3 <http://php.net/manual/en/book.sqlite3.php>`_
* `PHP BCMath <http://php.net/manual/en/book.bc.php>`_
* `PHP pthreads <http://pthreads.org/>`_  by `krakjoe <https://github.com/krakjoe>`_ : Threading for PHP - Share Nothing, Do Everything.
* `PHP YAML <https://code.google.com/p/php-yaml/>`_ by Bryan Davis: The Yaml PHP Extension provides a wrapper to the LibYAML library.
* `LibYAML <http://pyyaml.org/wiki/LibYAML>`_  by Kirill Simonov: A YAML 1.1 parser and emitter written in C.
* `cURL <http://curl.haxx.se/>`_ : cURL is a command line tool for transferring data with URL syntax
* `Zlib <http://www.zlib.net/>`_ : A Massively Spiffy Yet Delicately Unobtrusive Compression Library
* `Source RCON Protocol <https://developer.valvesoftware.com/wiki/Source_RCON_Protocol>`_
* `UT3 Query Protocol <http://wiki.unrealadmin.org/UT3_query_protocol>`_

.. _PHP-Windows: http://sourceforge.net/projects/pocketmine/files/windows/dev/
.. _PHP-SourceForge: http://sourceforge.net/projects/pocketmine/files/builds/
.. _PHP-Jenkins: http://jenkins.pocketmine.net/
.. _PM-Stable: https://github.com/PocketMine/PocketMine-MP/releases
.. _PM-Dev: http://jenkins.pocketmine.net/job/PocketMine-MP/
