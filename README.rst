PocketMine-MP Documentation
===============================

Documentation 
Requirements
------------

To build the documentation you need the following python modules:

* `sphinx-doc <http://sphinx-doc.org/>`_
* `Custom readthedocs theme <https://github.com/snide/sphinx_rtd_theme>`_

``pip install -r requirements.txt``

How to build the documentation
------------------------------

The generated docs will be in the build directory

.. code-block:: sh
	$ cd docs
	$ make html


TODO
----

* FAQ
    * add more information

* Installation
    * add missing os



    

